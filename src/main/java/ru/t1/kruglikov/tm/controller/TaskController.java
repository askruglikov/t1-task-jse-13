package ru.t1.kruglikov.tm.controller;


import ru.t1.kruglikov.tm.api.ITaskController;
import ru.t1.kruglikov.tm.api.ITaskService;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Task;
import ru.t1.kruglikov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name,description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task.getName() + ": " + task.getDescription());
        }
    }

    @Override
    public void showTask() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index + ". "+task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASKS CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        final Task task=taskService.findOneById(id);
        if (task == null) {
            System.out.println("[ERROR]");
            return;
        }
        displayTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        final Task task=taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[ERROR]");
            return;
        }
        displayTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void displayTask(final Task task) {
        if (task == null) return;
        System.out.println("[ID: ]"+task.getId());
        System.out.println("[NAME: ]"+task.getName());
        System.out.println("[DESCRIPTION: ]"+task.getDescription());
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status=Status.toStatus(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");

    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status=Status.toStatus(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        final Task task = taskService.changeStatusByIndex(index,Status.IN_PROGRESS);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id,Status.IN_PROGRESS);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index=TerminalUtil.nextNumber()-1;
        final Task task = taskService.changeStatusByIndex(index,Status.COMPLETED);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id=TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id,Status.COMPLETED);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
        System.out.println("[OK]");
    }

}
