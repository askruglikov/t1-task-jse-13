package ru.t1.kruglikov.tm.service;


import ru.t1.kruglikov.tm.api.ITaskRepository;
import ru.t1.kruglikov.tm.api.ITaskService;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name==null || name.isEmpty()) return null;
        if (description==null) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index > taskRepository.getSize()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

}
