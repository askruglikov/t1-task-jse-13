package ru.t1.kruglikov.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
