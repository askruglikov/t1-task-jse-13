package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusById(String id, Status status);

}
