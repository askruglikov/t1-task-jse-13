package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTask();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void displayTask(Task task);

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskById();

    void removeTaskByIndex();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

    void showTasksByProjectId();

}
